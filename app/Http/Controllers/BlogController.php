<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{

    public function index()
    {

    }

    public function show()
    {
        $title = "Posts by Juan";
        /*
        $faker = Faker\Factory::create();
        $posts = [];
        for ($i = 0; $i < 10; $i++) {
            $posts[] = $faker->text;

        }
        */

        $posts = DB::table('posts')->get();

        //dd($posts);

        return view('blog', ['posts' => $posts, 'title' => $title]);
    }
}
