<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = App\Comment::all();
    }


    /**
     * @param Request $request
     * @param $id
     */
    public function create(Request $request, $id)
    {
        $request->validate([
            'email' => 'required',
            'comment' => 'required|max:150',
        ]);

        $comment = new Comment;
        $comment->post_id = $id;
        $comment->comment_id = null;
        $comment->comment = $request->input('comment','');
        $comment->email = $request->input('email', '');
        $comment->isResponse = false;

        $comment->save();

        return redirect()->route('post', ['id' => $id]);

    }

    public function showCreate($id){
        return view('create', ['id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();

        return redirect()->route('post', ['id' => $id]);
    }
}
