<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [''];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function children() {
        return $this->hasMany('App\Comment');
    }
    public function parent() {
        return $this->belongsTo('App\Comment');
    }
}
