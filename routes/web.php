<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/contact', 'ContactController@index')->name('contact');

Route::get('/blog', 'BlogController@show')->name('blog');

Route::post('/send', 'ContactController@process')->name('send');

Route::any('/post/{id}', 'PostController@index')->name('post');

Route::get('/showCreate/{id}', 'CommentController@showCreate')->name('showCreate');

Route::post('/create/{id}', 'CommentController@create')->name('create');

Route::get('/delete/{id}', 'CommentController@destroy')->name('delete');
