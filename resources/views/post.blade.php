@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-bottom: 50px;"><h1>{{ $post->title }}</h1></div>
            <div class="row">
                    <div class="col-md-12">
                        <h4>{{ $post->article }}</h4>
                        <p>{{ $post->body }}</p>
                    </div>
                    <a href="{{route('showCreate',['id' => $post->id])}}">Crear comentario</a>
            </div>

            <div class="row" style="margin-top: 40px;">

                @foreach($comments as $comment)

                    <div class="col-md-12" style="border: 1px solid black; background-color: #ccc; margin-bottom: 15px;">
                        <h4>{{ $comment->email }}</h4><br>
                        <p>{{ $comment->comment }}</p>
                        <br>
                        <a href="{{route('delete', ['id' => $comment->id])}}">Eliminar</a>
                    </div>

                @endforeach

                <div class="col-md-12">

                </div>

            </div>
        </div>

    </div>

@endsection
