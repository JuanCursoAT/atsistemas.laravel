@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12 text-center" style="margin-bottom: 50px;"><h1>Crea un comentario</h1></div>
            <div class="row">

                {{ Form::open(['route' => ['create', $id], 'method' => 'post']) }}

                @csrf

                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="col-md-12"> {{ Form::label('email', 'E-Mail Address') }} <br> {{ Form::text('email', 'example@gmail.com') }}</div>
                <br>
                @error('comment')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="col-md-12">{{ Form::label('comment', 'Comentario') }} <br> {{ Form::textarea('comment', '') }} </div>
                <br><div class="col-md-12"> {{ Form::submit('Enviar') }}</div>

                {{ Form::close() }}

            </div>
        </div>

    </div>



@endsection
