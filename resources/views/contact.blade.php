@extends('layouts.app')

<style>

    * {
        margin: 0;
        padding: 0;
    }

    body {
        font-family: "Roboto", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif, aclonica;
        font-size: 14px;
        color: white;
    }

    .h3 {
        font-family: "aclonica";
        color: orange;
    }

</style>

@section('content')

    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Formulario de Contacto</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
    <section class="container">
        <div class="row">
            <h3 class="center-align">CONTACTO</h3>
            <article class="col s6 offset-s3">
                <form method="POST" action="{{route('send')}}">

                    @csrf

                    <div class="input-field">
                        <i class="material-icons prefix">perm_identity</i>
                        <label for="name">Nombre</label>
                        <input type="text" name="name">
                    </div>

                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="input-field">
                        <i class="material-icons prefix">email</i>
                        <label for="email">Email</label>
                        <input type="email" name="email">
                    </div>

                    @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="input-field">
                        <i class="material-icons prefix">mode_edit</i>
                        <label for="mensaje">Mensaje</label>
                        <textarea name="mensaje" id="" rows="10" class="materialize-textarea" length="140"></textarea>
                    </div>

                    <p class="center-align">
                        <button class="waves-effect waves-light btn" type="submit"><i
                                    class="material-icons right">send</i>enviar
                        </button>
                    </p>

                </form>

            </article>
        </div>
    </section>


    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
    </body>
    </html>

@endsection
