@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-md-12 text-center" style="margin-bottom: 50px;"><h1>{{ $title }}</h1></div>
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-6">
                        <h2>{{ $post->title }}</h2>
                        <h4>{{ $post->article }}</h4>
                        <p>{{ $post->body }}</p>
                        <a href="{{route('post',$post->id)}}">Leer más..</a>
                    </div>
                @endforeach
            </div>
        </div>

    </div>

@endsection
